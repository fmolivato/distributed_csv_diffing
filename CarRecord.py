from dataclasses import dataclass
from datetime import datetime


class ParseFieldsError(Exception):
    """Class that rapresents errors occurred during the parsing"""

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


@dataclass
class CarRecord:
    """Rapresentation of a single record of the dataset"""

    cap: int = None
    date: datetime = None
    fuel: str = None
    make: str = None
    url1: str = None
    year: int = None
    color: str = None
    doors: int = None
    model: str = None
    price: int = None
    setup: str = None
    color2: str = None
    doors2: str = None
    country: str = None
    gearbox: str = None
    idOffer: str = None
    mileage: int = None
    version: str = None
    auRating: int = None
    bodyType: str = None
    features: str = None
    advertId1: str = None
    bodyType2: str = None
    modelType: str = None
    daysOnline: int = None
    driveTrain: str = None
    fourWheels: str = None
    horsepower: int = None
    scoreLabel: str = None
    sellerCity: str = None
    sellerName: str = None
    shouldCost: str = None
    engine_size: float = None
    channelSales: str = None
    dataSourceName1: str = None
    modelGeneration: str = None
    priceDifference: str = None
    priceRatingScore1: str = None
    priceRatingScore2: str = None
    priceRatingScore3: str = None
    priceRatingScore4: str = None
    priceRatingScore5: str = None
    lastPriceChangedAt: datetime = None
    variationData1: str = None
    variationData2: str = None
    variationPrice1: str = None
    variationPrice2: str = None
    variationData3: str = None
    variationPrice3: str = None
    url2: str = None
    advertId2: str = None
    dataSourceName2: str = None
    url3: str = None
    advertId3: str = None
    dataSourceName3: str = None
    url4: str = None
    advertId4: str = None
    dataSourceName4: str = None
    closed: str = None
    id_redirect: str = None
    # per riconoscere gli oggetti a cui non è ancora stato assegnato uno status
    status: str = None

    def __post_init__(self):
        self.date = self.parse_all_datetimes(self.date)

    def parse_all_datetimes(self, date: str) -> datetime:
        """Try to parse all the fields that should be datetime

        Args:
            date (str): a string that should be a date

        Raises:
            ParseFieldsError: error that occur during the CarRecord parsing of data

        Returns:
            datetime: parsed value
        """
        try:
            date = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError as e:
            raise ParseFieldsError(e)
        return date

    def __eq__(self, obj: object) -> bool:
        """Custom dunder method that describe the actual equality between
        records as an equality between 'idOffer' fields

        Args:
            obj (object): object to compare

        Raises:
            TypeError: error raised when the object is not comparable because of the type

        Returns:
            bool: whether is equal or not
        """
        if isinstance(obj, CarRecord):
            return (
                True
                if self.idOffer == obj.idOffer and self.date.date() == obj.date.date()
                else False
            )
        else:
            raise TypeError(f"Expected a CarRecord object instead got { type(obj) }")

    def __lt__(self, obj: object):
        """Custom dunder method that describe both the equality of 'idOffer' fields
        and the comparison of the 'date' fileds

        Args:
            obj (object): object to compare

        Returns:
            bool: whether is lower or not
        """
        if self.__eq__(obj):
            return (
                True if self.idOffer == obj.idOffer and self.date < obj.date else False
            )

    def __gt__(self, obj: object):
        """Custom dunder method that describe both the equality of 'idOffer' fields
        and the comparison of the 'date' fileds

        Args:
            obj (object): object to compare

        Returns:
            bool: whether is greater or not
        """
        if self.__eq__(obj):
            return (
                True if self.idOffer == obj.idOffer and self.date > obj.date else False
            )

    def __sub__(self, obj):
        if isinstance(obj, CarRecord):
            if self.idOffer == obj.idOffer:

                if self.price != obj.price:
                    self.status = "PRICEUPDATE"
                elif self.closed == "true" and self.id_redirect == None:
                    self.status = "CLOSED"
                elif (
                    (obj.status == "CLOSED" or self.id_redirect != obj.id_redirect)
                    and self.closed == "true"
                    and self.id_redirect != None
                ):
                    self.status = "DUPLICATED"
                elif obj.status == "CLOSED" and self.closed == None:
                    self.status = "REOPEN"
                else:  # se non ci sono variazioni
                    self.status = "OPEN"

                return self
            else:
                ValueError(f"Expected a CarRecord with same idOffer")
        else:
            raise TypeError(f"Expected a CarRecord object instead got { type(obj) }")
