from datetime import datetime, timedelta
import sys
import os
import pytest

# getting the name of the directory
# where the this file is present.
current = os.path.dirname(os.path.realpath(__file__))

# Getting the parent directory name
# where the current directory is present.
parent = os.path.dirname(current)

# adding the parent directory to
# the sys.path.
sys.path.append(parent)

# now we can import the module in the parent
# directory.
from CarRecord import CarRecord, ParseFieldsError


class TestCarRecord:
    """It tests the Record class"""

    @pytest.fixture
    def car(self):
        """Build a simple correct record that shouldn't raise errors

        Returns:
            dict: fake car record data
        """
        car_record = {
            "cap": "34",
            "date": "2020-06-19T07:42:54.350Z",
            "fuel": None,
            "make": None,
            "url1": None,
            "year": "2002",
            "color": None,
            "doors": "2",
            "model": None,
            "price": "122000",
            "setup": None,
            "color2": None,
            "doors2": None,
            "country": None,
            "gearbox": None,
            "idOffer": "1345678",
            "mileage": "50000",
            "version": None,
            "auRating": "3",
            "bodyType": None,
            "features": None,
            "advertId1": None,
            "bodyType2": None,
            "modelType": None,
            "daysOnline": "2",
            "driveTrain": None,
            "fourWheels": None,
            "horsepower": "560",
            "scoreLabel": None,
            "sellerCity": None,
            "sellerName": None,
            "shouldCost": None,
            "engine_size": "3.9",
            "channelSales": None,
            "dataSourceName1": None,
            "modelGeneration": None,
            "priceDifference": None,
            "priceRatingScore1": None,
            "priceRatingScore2": None,
            "priceRatingScore3": None,
            "priceRatingScore4": None,
            "priceRatingScore5": None,
            "lastPriceChangedAt": "2020-06-19T07:39:54.984Z",
            "variationData1": None,
            "variationData2": None,
            "variationPrice1": None,
            "variationPrice2": None,
            "variationData3": None,
            "variationPrice3": None,
            "url2": None,
            "advertId2": None,
            "dataSourceName2": None,
            "url3": None,
            "advertId3": None,
            "dataSourceName3": None,
            "url4": None,
            "advertId4": None,
            "dataSourceName4": None,
            "closed": None,
            "id_redirect": None,
            "status": None,
        }
        return car_record

    def test_parse_all_datetimes(self, car: dict):
        """Tests the parse_all_datetimes

        Args:
            car (dict): simple car data to verify
        """
        # SETUP
        wrong_car_date = car.copy()
        wrong_date = "ciao"
        wrong_car_date["date"] = wrong_date

        exp_date = datetime.strptime(car["date"], "%Y-%m-%dT%H:%M:%S.%fZ")

        # EXERCISE
        res_date = CarRecord.parse_all_datetimes(None, car["date"])

        with pytest.raises(ParseFieldsError) as err:
            CarRecord.parse_all_datetimes(None, wrong_car_date["date"])

        # VERIFY
        assert res_date == exp_date
        assert (
            str(err.value)
            == f"time data '{wrong_date}' does not match format '%Y-%m-%dT%H:%M:%S.%fZ'"
        )

        # CLEANUP
        # none

    def test_eq(self, car: dict):
        """Tests the dunder method __eq__

        Args:
            car (dict): simple car data to verify
        """
        # SETUP
        car1_record = CarRecord(*car.copy().values())

        car2 = car.copy()
        car2["idOffer"] = "".join(reversed(car["idOffer"]))
        print(car["idOffer"])
        print(car2["idOffer"])
        car2_record = CarRecord(*car2.values())
        print(car2_record.idOffer)

        # EXERCISE
        res_equal = car1_record == car1_record
        res_not_equal = car1_record == car2_record

        # VERIFY
        assert res_equal == True
        assert res_not_equal == False

        # CLEANUP
        # none

    def test_lt(self, car: dict):
        """Tests the dunder method __lt__

        Args:
            car (dict): simple car data to verify
        """
        # SETUP
        car_record = CarRecord(*car.copy().values())

        car_record_greater = CarRecord(*car.copy().values())
        car_record_greater.date += timedelta(days=10)

        car_record_lower = CarRecord(*car.copy().values())
        car_record_lower.date -= timedelta(days=10)

        # EXERCISE
        res_equal = car_record.date < car_record.date
        res_lower = car_record_lower.date < car_record.date
        res_greater = car_record_greater.date < car_record.date

        # VERIFY
        assert res_lower == True
        assert res_equal == False
        assert res_greater == False

        # CLEANUP
        # none

    def test_gt(self, car: dict):
        """Tests the dunder method __gt__

        Args:
            car (dict): simple car data to verify
        """
        # SETUP
        car_record = CarRecord(*car.copy().values())

        car_record_greater = CarRecord(*car.copy().values())
        car_record_greater.date += timedelta(days=10)

        car_record_lower = CarRecord(*car.copy().values())
        car_record_lower.date -= timedelta(days=10)

        # EXERCISE
        res_equal = car_record.date > car_record.date
        res_lower = car_record_lower.date > car_record.date
        res_greater = car_record_greater.date > car_record.date

        # VERIFY
        assert res_greater == True
        assert res_equal == False
        assert res_lower == False

        # CLEANUP
        # none

    def test_sub(self, car: dict):
        """Tests the dunder method __sub__, that is customized to show the changes between objects

        Args:
            car (dict): simple car data to verify
        """
        car_obj = CarRecord(**car)

        # SETUP
        price_update = CarRecord(**car)
        price_update.price = float(price_update.price) - 1

        closed = CarRecord(**car)
        closed.closed = "true"

        duplicated = CarRecord(**car)
        duplicated.closed = "true"
        duplicated.id_redirect = "12345"

        reopen = CarRecord(**car)
        reopen.closed = None
        closed.status = "CLOSED"

        # EXERCISE
        res_reopen = reopen - closed
        res_price_update = price_update - car_obj
        res_closed = closed - car_obj
        res_duplicated = duplicated - car_obj
        res_no_changes = car_obj - car_obj

        exp_no_changes = res_no_changes.status

        # VERIFY
        assert res_reopen.status == "REOPEN"
        assert res_price_update.status == "PRICEUPDATE"
        assert res_closed.status == "CLOSED"
        assert res_duplicated.status == "DUPLICATED"
        assert res_no_changes.status == exp_no_changes

        # CLEANUP
        # none
