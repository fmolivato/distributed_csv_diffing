from typing import List
from pyspark.sql import SparkSession
from copy import deepcopy
from datetime import timedelta
import sys
import os
import pandas as pd
import pytest

# getting the name of the directory
# where the this file is present.
current = os.path.dirname(os.path.realpath(__file__))

# Getting the parent directory name
# where the current directory is present.
parent = os.path.dirname(current)

# adding the parent directory to
# the sys.path.
sys.path.append(parent)

# now we can import the module in the parent
# directory.
from Differential import Differential
from CarRecord import CarRecord


class TestDifferential:
    """It tests the Differential class"""

    @pytest.fixture
    def spark_session(self) -> SparkSession:
        """Build a spark session"""
        return (
            SparkSession.builder.appName("Differential_test")
            .master("local[*]")
            .getOrCreate()
        )

    @pytest.fixture
    def empty_record(self, spark_session: SparkSession) -> List[CarRecord]:
        """Build an empty dataframe. When df() is compared with this as reference
        it should be produced an OPEN status.
        Instead when this is compared with df() as reference it should be
        produced a CLOSED status.

        Args:
            spark_session (SparkSession): spark session

        Returns:
            List[CarRecord]: lists of record
        """
        return []

    @pytest.fixture
    def record(self, spark_session: SparkSession) -> List[CarRecord]:
        """Build a dataframe with just the important columns that will be used to assign a status

        Args:
            spark_session (SparkSession): spark session

        Returns:
            List[CarRecord]: lists of record
        """
        record_prototype = {
            "idOffer": "10",
            "price": "122000",
            "date": "2020-06-19T07:42:54.350Z",
            "id_redirect": None,
            "closed": None,
        }

        return [CarRecord(**record_prototype)]

    @pytest.fixture
    def price_update_record(self, spark_session: SparkSession) -> List[CarRecord]:
        """Build a dataframe that compared to df() should produce a PRICEUPDATE status

        Args:
            spark_session (SparkSession): spark session

        Returns:
            List[CarRecord]: lists of record
        """
        # this date comes later than the one of 'rdd'
        record_prototype = {
            "idOffer": "10",
            "price": "10",
            "date": "2020-06-29T07:42:54.350Z",
            "id_redirect": None,
            "closed": None,
        }

        return [CarRecord(**record_prototype)]

    @pytest.fixture
    def closed_record(self, spark_session: SparkSession) -> List[CarRecord]:
        """Build a dataframe that compared to df() should produce a CLOSED status

        Args:
            spark_session (SparkSession): spark session

        Returns:
            List[CarRecord]: lists of record
        """
        # this date comes later than the one of 'rdd'
        record_prototype = {
            "idOffer": "10",
            "price": "122000",
            "date": "2020-06-29T07:42:54.350Z",
            "id_redirect": None,
            "closed": "true",
        }

        return [CarRecord(**record_prototype)]

    @pytest.fixture
    def duplicated_record(self, spark_session: SparkSession) -> List[CarRecord]:
        """Build a dataframe that compared to df() should produce a DUPLICATED status

        Args:
            spark_session (SparkSession): spark session

        Returns:
            List[CarRecord]: lists of record
        """
        # this date comes later than the one of 'rdd'
        record_prototype = {
            "idOffer": "10",
            "price": "122000",
            "date": "2020-06-29T07:42:54.350Z",
            "id_redirect": "55",
            "closed": "true",
        }

        return [CarRecord(**record_prototype)]

    def test_duplicated(
        self, spark_session: SparkSession, record: list, duplicated_record: list
    ):
        """Tests whether the diffing process catch records duplication

        Args:
            spark_session (SparkSession): spark session
            record (list): standard record
            duplicated_record (list): record that simulate the duplication
        """
        # SETUP
        records = record + duplicated_record
        reverse_records = duplicated_record + record

        rdd = spark_session.sparkContext.parallelize(records)
        reverse_rdd = spark_session.sparkContext.parallelize(reverse_records)

        # EXERCISE
        res = Differential.same_records_diff(None, rdd).collect()
        res_inverted = Differential.same_records_diff(None, reverse_rdd).collect()

        # VERIFY
        assert res[0].status == "DUPLICATED"
        assert res_inverted[0].status == "DUPLICATED"

        # CLEANUP
        # none

    def test_closed(
        self, spark_session: SparkSession, record: list, closed_record: list
    ):
        """Tests whether the diffing process catch closed records

        Args:
            spark_session (SparkSession): spark session
            record (list): standard record
            closed_record (list): record that simulate a closed record
        """
        # SETUP
        records = record + closed_record
        reverse_records = closed_record + record

        rdd = spark_session.sparkContext.parallelize(records)
        reverse_rdd = spark_session.sparkContext.parallelize(reverse_records)

        # EXERCISE
        res = Differential.same_records_diff(None, rdd).collect()
        res_inverted = Differential.same_records_diff(None, reverse_rdd).collect()

        # VERIFY
        assert res[0].status == "CLOSED"
        assert res_inverted[0].status == "CLOSED"

        # CLEANUP
        # none

    def test_price_update(
        self, spark_session: SparkSession, record: list, price_update_record: list
    ):
        """Tests whether the diffing process catch price update on records

        Args:
            spark_session (SparkSession): spark session
            record (list): standard record
            price_update_record (list): record that simulate the price update
        """
        # SETUP
        records = record + price_update_record
        reverse_records = price_update_record + record

        rdd = spark_session.sparkContext.parallelize(records)
        reverse_rdd = spark_session.sparkContext.parallelize(reverse_records)

        # EXERCISE
        res = Differential.same_records_diff(None, rdd).collect()
        res_inverted = Differential.same_records_diff(None, reverse_rdd).collect()

        # VERIFY
        assert res[0].status == "PRICEUPDATE"
        assert res_inverted[0].status == "PRICEUPDATE"

        # CLEANUP
        # none

    def test_reopen(self, spark_session: SparkSession, record: list):
        """Tests whether the diffing process catch the reopening of records

        Args:
            spark_session (SparkSession): spark session
            record (list): standard record
        """
        # SETUP
        closed_status_record = record.copy()
        closed_status_record[0].status = "CLOSED"
        closed_status_record[0].date -= timedelta(days=10)

        records = record + closed_status_record
        reverse_records = closed_status_record + record

        rdd = spark_session.sparkContext.parallelize(records)
        reverse_rdd = spark_session.sparkContext.parallelize(reverse_records)

        # EXERCISE
        res = Differential.same_records_diff(None, rdd).collect()
        res_inverted = Differential.same_records_diff(None, reverse_rdd).collect()

        # VERIFY
        assert res[0].status == "REOPEN"
        assert res_inverted[0].status == "REOPEN"

        # CLEANUP
        # none

    def test_extract_never_seen_records(
        self, spark_session: SparkSession, empty_record: list, record: list
    ):
        """Tests whether the diffing process catch never seen record

        Args:
            spark_session (SparkSession): spark session
            empty_record (list): empty record
            record (list): standard record
        """
        # SETUP
        rdd_empty = spark_session.sparkContext.parallelize(empty_record)
        rdd = spark_session.sparkContext.parallelize(record)

        # EXERCISE
        res = Differential.extract_never_seen_records(None, rdd, rdd_empty).collect()

        # VERIFY
        assert res == record

        # CLEANUP
        # none

    def test_extract_records_not_updated(
        self, spark_session: SparkSession, empty_record: list, record: list
    ):
        """Tests whether the diffing process catch not updated record

        Args:
            spark_session (SparkSession): spark session
            empty_record (list): empty record
            record (list): standard record
        """
        # SETUP
        rdd_empty = spark_session.sparkContext.parallelize(empty_record)
        rdd = spark_session.sparkContext.parallelize(record)

        # EXERCISE
        res = Differential.extract_records_disappeared(None, rdd_empty, rdd).collect()

        # VERIFY
        assert res == record

        # CLEANUP
        # none

    def test_extract_shared_records(
        self, spark_session: SparkSession, price_update_record: list, record: list
    ):
        """Tests whether the diffing process extract shared records

        Args:
            spark_session (SparkSession): spark session
            price_update_record (list): record that simulate the price update
            record (list): standard record
        """
        # SETUP
        rdd_price_update = spark_session.sparkContext.parallelize(price_update_record)
        rdd = spark_session.sparkContext.parallelize(record)

        exp_rdd = spark_session.sparkContext.parallelize(
            record + price_update_record
        ).collect()

        # EXERCISE
        res = Differential.extract_shared_records(None, rdd_price_update, rdd).collect()

        # VERIFY
        assert res == exp_rdd

        # CLEANUP
        # none

    def test_update_status(
        self,
        spark_session: SparkSession,
        closed_record: List[CarRecord],
        duplicated_record: List[CarRecord],
        price_update_record: List[CarRecord],
        record: List[CarRecord],
    ):
        """Tests whether the status of reference data is updated correctly

        Args:
            spark_session (SparkSession): spark session
            closed_record (List[CarRecord]): simulate a closed record
            duplicated_record (List[CarRecord]): simulate a duplicated record
            price_update_record (List[CarRecord]): simulate a price update of a record
            record (List[CarRecord]): standard record
        """
        # SETUP
        record_to_close = deepcopy(record)
        record_to_duplicate = deepcopy(record)
        record_to_change_price = deepcopy(record)
        record_not_updated = deepcopy(record)
        record_already_closed = deepcopy(record)
        record_unchanged = deepcopy(record)

        record_to_close[0].idOffer = 1
        record_to_duplicate[0].idOffer = 2
        record_to_change_price[0].idOffer = 3
        record_not_updated[0].idOffer = 4
        record_already_closed[0].idOffer = 5
        record_unchanged[0].idOffer = 6

        record_to_close[0].status = "OPEN"
        record_to_duplicate[0].status = "OPEN"
        record_to_change_price[0].status = "OPEN"
        record_not_updated[0].status = "OPEN"
        record_already_closed[0].status = "CLOSED"
        record_unchanged[0].status = "OPEN"

        reference_data = (
            record_to_close
            + record_to_duplicate
            + record_to_change_price
            + record_not_updated
            + record_already_closed
            + record_unchanged
        )

        record_new = deepcopy(record)
        record_new_duplicated = deepcopy(record)
        record_new_closed = deepcopy(record)
        record_to_reopen = deepcopy(record_already_closed)

        record_new[0].idOffer = 7
        record_new_duplicated[0].idOffer = 8
        record_new_closed[0].idOffer = 9
        record_to_reopen[0].idOffer = record_already_closed[0].idOffer
        closed_record[0].idOffer = record_to_close[0].idOffer
        duplicated_record[0].idOffer = record_to_duplicate[0].idOffer
        price_update_record[0].idOffer = record_to_change_price[0].idOffer

        record_new[0].date += timedelta(days=10)
        record_to_reopen[0].date += timedelta(days=10)

        record_new_duplicated[0].closed = "true"
        record_new_duplicated[0].id_redirect = "qualcosa"

        record_new_closed[0].closed = "true"

        new_data = (
            closed_record
            + duplicated_record
            + price_update_record
            + record_to_reopen
            + record_unchanged
            + record_new
            + record_new_duplicated
            + record_new_closed
        )

        record_new[0].status = "OPEN"
        record_to_reopen[0].status = "REOPEN"
        closed_record[0].status = "CLOSED"
        duplicated_record[0].status = "DUPLICATED"
        price_update_record[0].status = "PRICEUPDATE"
        record_not_updated[0].status = "CLOSED"
        record_new_duplicated[0].status = "DUPLICATED"
        record_new_closed[0].status = "CLOSED"

        exp_shared = sorted(
            record_to_reopen
            + closed_record
            + duplicated_record
            + price_update_record
            + record_unchanged,
            key=lambda obj: obj.idOffer,
        )

        exp_disappeared = sorted(
            record_not_updated,
            key=lambda obj: obj.idOffer,
        )

        exp_never_seen = sorted(
            record_new + record_new_duplicated + record_new_closed,
            key=lambda obj: obj.idOffer,
        )

        new_rdd = spark_session.sparkContext.parallelize(new_data)
        reference_rdd = spark_session.sparkContext.parallelize(reference_data)

        differentiator = Differential(spark_session)

        # EXERCISE
        shared, never_seen, disappeared = differentiator.update_status(
            new_rdd, reference_rdd
        )
        res_shared = sorted(shared.collect(), key=lambda obj: obj.idOffer)
        res_never_seen = sorted(never_seen.collect(), key=lambda obj: obj.idOffer)
        res_disappeared = sorted(
            disappeared.collect(),
            key=lambda obj: obj.idOffer,
        )

        # VERIFY
        assert res_shared == exp_shared
        assert res_never_seen == exp_never_seen
        assert res_disappeared == exp_disappeared

        # CLEANUP
        # none

    def test_get_just_updates(
        self,
        spark_session: SparkSession,
        closed_record: List[CarRecord],
        duplicated_record: List[CarRecord],
        record: List[CarRecord],
    ):
        """Tests whether the updates are extracted correctly

        Args:
            spark_session (SparkSession): spark session
            closed_record (List[CarRecord]): simulate a closed record
            record (List[CarRecord]): standard record
        """
        # SETUP
        record[0].status = "OPEN"
        closed_record[0].status = "CLOSED"
        new_record = deepcopy(record)
        new_record[0].idOffer = int(record[0].idOffer) + 1

        # duplicated -> closed non deve generare update
        duplicated_record[0].status = "DUPLICATED"

        # reopen -> open non deve generare update
        reopen_record = deepcopy(record)
        reopen_record[0].status = "REOPEN"

        old_data = new_record + record
        old_data_duplicated = new_record + duplicated_record
        old_data_reopen = new_record + reopen_record

        # genero i nuovi dati per tutte le casistiche
        closed_record[0].date += timedelta(days=10)
        late_record = deepcopy(record)
        late_record[0].date += timedelta(days=10)
        new_record[0].date += timedelta(days=10)

        new_data_closed = new_record + closed_record
        new_data_duplicated = new_record + closed_record
        new_data_reopen = new_record + late_record

        new_rdd_closed = spark_session.sparkContext.parallelize(new_data_closed)
        new_rdd_duplicated = spark_session.sparkContext.parallelize(new_data_duplicated)
        new_rdd_reopen = spark_session.sparkContext.parallelize(new_data_reopen)
        old_rdd = spark_session.sparkContext.parallelize(old_data)
        old_rdd_duplicated = spark_session.sparkContext.parallelize(old_data_duplicated)
        old_rdd_reopen = spark_session.sparkContext.parallelize(old_data_reopen)

        # EXERCISE
        res = Differential.get_just_updates(None, new_rdd_closed, old_rdd).collect()
        res_duplicated = Differential.get_just_updates(
            None, new_rdd_duplicated, old_rdd_duplicated
        ).collect()
        res_reopen = Differential.get_just_updates(
            None, new_rdd_reopen, old_rdd_reopen
        ).collect()

        # VERIFY
        assert res == closed_record
        assert res_duplicated == []
        assert res_reopen == []

        # CLEANUP
        # none
        pass

    def test_get_just_new_disappeared(
        self,
        spark_session: SparkSession,
        closed_record: List[CarRecord],
        duplicated_record: List[CarRecord],
        record: List[CarRecord],
    ):
        """Tests whether the returned value is extracted correctly

        Args:
            spark_session (SparkSession): spark session
            closed_record (List[CarRecord]): simulate a closed record
            record (List[CarRecord]): standard record
        """
        # SETUP
        closed_index = 1
        duplicated_index = 2

        record[0].status = "OPEN"
        closed_record[0].idOffer = int(record[0].idOffer) + closed_index
        closed_record[0].status = "CLOSED"
        duplicated_record[0].idOffer = int(record[0].idOffer) + duplicated_index
        duplicated_record[0].status = "DUPLICATED"
        disappeared_record = deepcopy(record)
        disappeared_record[0].status = "CLOSED"

        old_data = record + closed_record + duplicated_record
        disappeared_data = disappeared_record + closed_record + duplicated_record
        old_rdd = spark_session.sparkContext.parallelize(old_data)
        disappeared_rdd = spark_session.sparkContext.parallelize(disappeared_data)

        # expected
        exp_res = disappeared_record

        # EXERCISE
        res = Differential.get_just_new_disappeared(
            None, old_rdd, disappeared_rdd
        ).collect()

        # VERIFY
        assert res == exp_res

        # CLEANUP
        # none
        pass

    def test_check_filename(self):
        """Tests whether a filename exists"""
        # SETUP
        filename = "test_check_filename.txt"

        with open(filename, "w+") as file:
            file.write("testo casuale")

        # EXERCISE
        res = Differential.check_filename(None, filename)
        res_not_present = Differential.check_filename(None, "file_non_esistente")

        # VERIFY
        assert res == True
        assert res_not_present == False

        # CLEANUP
        os.remove(filename)

    def test_save_data(self, spark_session: SparkSession, record: List[CarRecord]):
        """Tests whether the data are save correctly

        Args:
            spark_session (SparkSession): spark session
            record (List[CarRecord]): standard record
        """
        # SETUP
        filename = "test_save_data.csv"
        differentiator = Differential(spark_session)

        rdd = spark_session.sparkContext.parallelize(record)

        # EXERCISE
        differentiator.save_file(rdd, filename)

        # VERIFY
        assert filename in os.listdir()

        # CLEANUP
        os.remove(filename)

    def test_read_file(self, spark_session: SparkSession, record: List[CarRecord]):
        """Tests whether a file is saved correctly

        Args:
            spark_session (SparkSession): spark session
            record (List[CarRecord]): standard record
        """
        # SETUP
        filename = "test_read.csv"
        differentiator = Differential(spark_session)

        rdd = spark_session.sparkContext.parallelize(record)
        differentiator.save_file(rdd, filename)

        # EXERCISE
        res = differentiator.read_file(filename).collect()

        # VERIFY
        assert res == record

        # CLEANUP
        os.remove(filename)

    def test_run_diff(
        filename,
        spark_session: SparkSession,
        price_update_record: List[CarRecord],
        record: List[CarRecord],
    ):
        """Teste whether the diffing process integrates correctly all
        the cases (open, closed, duplicate, price update, reopen)

        Args:
            filename ([type]): input file
            spark_session (SparkSession): spark session
            price_update_record (List[CarRecord]): simulate a price update of a record
            record (List[CarRecord]): standard record
        """
        # SETUP
        new_data_filename = "new_data.csv"
        local_storage_filename = "local.csv"
        updates_filname = "updates.csv"

        record_data = record[0].__dict__
        record_data["date"] = record_data["date"].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        record_data["status"] = "OPEN"
        pd.DataFrame([record_data]).to_csv(local_storage_filename, index=False)

        price_data = price_update_record[0].__dict__
        price_data["date"] = price_data["date"].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        pd.DataFrame([price_data]).to_csv(new_data_filename, index=False)
        diff = Differential(spark_session)

        # EXERCISE
        diff.run_diff(new_data_filename, local_storage_filename, updates_filname)

        # VERIFY
        res = pd.read_csv(updates_filname)
        assert CarRecord(*res.to_numpy()[0]).status == "PRICEUPDATE"

        # CLEANUP
        # none
        pass
