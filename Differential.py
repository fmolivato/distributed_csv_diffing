from pyspark.sql import SparkSession
from pyspark import RDD
from CarRecord import CarRecord
import os
import pandas as pd


class Differential:
    """Check how some records change status between two given pyspark RDD.
    It uses CarRecord for records comparing.
    Than it generates a file that contains only updated records.
    """

    def __init__(self, session: SparkSession):
        self.session = session

    def run_diff(
        self, filename_new_data: str, filename_local_storage: str, filename_updates: str
    ) -> None:
        """Checks the updates of the car records in the new_rdd and saves both
        new_reference_rdd on file and the the updated records for furthur use

        Args:
            filename_new_data (str): file that keeps the new data to analyze
            filename_local_storage (str): name of the file that is the current status reference
            filename_updates (str): file in which should be saved the updated records
        """
        self.new_rdd = self.read_file(filename_new_data)
        self.reference_rdd = self.read_file(filename_local_storage)

        shared, never_seen, disappeared = self.update_status(
            self.new_rdd, self.reference_rdd
        )
        new_reference_rdd = shared.union(never_seen).union(disappeared)

        updated_shared = self.get_just_updates(new_reference_rdd, self.reference_rdd)
        only_new_disappeared = self.get_just_new_disappeared(
            self.reference_rdd, disappeared
        )
        # FIXME filtrare disappeared da campi gia closed oppure duplicated
        updated_records = updated_shared.union(never_seen).union(only_new_disappeared)

        self.save_file(updated_records, filename_updates)
        self.save_file(new_reference_rdd, filename_local_storage)

    def check_filename(self, filename: str) -> bool:
        """Verify the existence of the file

        Args:
            filename (str): name of the file

        Returns:
            bool: whether the file is present or not
        """
        return os.path.exists(filename)

    def save_file(self, rdd: RDD, filename: str) -> None:
        """Save an rdd to file using pandas (could lead to OOM if pandas dataframe is too big)

        Args:
            rdd (RDD): car records to save
            filename (str): name of the file
        """

        def to_dict(record: CarRecord):
            record_dict = record.__dict__
            record_dict["date"] = record_dict["date"].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            return record_dict

        # ATTENZIONE Pandas puo reggere una mole limitata di dati
        data = rdd.map(to_dict).collect()
        pd.DataFrame(data).to_csv(filename, index=False)

    def read_file(self, filename: str) -> RDD:
        """Read a csv file

        Args:
            filename (str): name of the file

        Returns:
            RDD: keeps car records
        """
        rdd = None

        if self.check_filename(filename):
            rdd = self.session.read.csv(filename, header=True).rdd.map(
                lambda row: CarRecord(**row.asDict())
            )
        else:
            rdd = self.session.sparkContext.parallelize([])

        return rdd

    def update_status(self, new_rdd: RDD, reference_rdd: RDD) -> tuple:
        """Perform a comparison between new_rdd and reference_rdd updating the status of the
        changed records

        Args:
            new_rdd (RDD): keeps the new data
            reference_rdd (RDD): keeps the old data

        Returns:
            tuple: diffed shared records, never seen ones and not updated
        """

        def new_car_record(car_record: CarRecord):
            """New cars has always status open by default, it is changed when flag closed="true"
            or whether closed and id_redirect are both present"""

            if car_record.closed == "true" and car_record.id_redirect is not None:
                car_record.status = "DUPLICATED"
            elif car_record.closed == "true":
                car_record.status = "CLOSED"
            else:
                car_record.status = "OPEN"

            return car_record

        def old_car_record(car_record: CarRecord):
            if car_record.status != "CLOSED" and car_record.status != "DUPLICATED":
                car_record.status = "CLOSED"
            return car_record

        never_seen = self.extract_never_seen_records(new_rdd, reference_rdd).map(
            new_car_record
        )

        disappeared = self.extract_records_disappeared(new_rdd, reference_rdd).map(
            old_car_record
        )

        shared = self.extract_shared_records(new_rdd, reference_rdd)
        shared_diff = self.same_records_diff(shared)

        return shared_diff, never_seen, disappeared

    def extract_never_seen_records(self, new_rdd: RDD, reference_rdd: RDD) -> RDD:
        """Extract all the new records (never seen in the past) from the new dataframe

        Args:
            new_rdd (RDD): new data
            reference_rdd (RDD): storage that keep the current status of a record

        Returns:
            RDD: all the records not in reference_rdd
        """

        new_rdd_idOffer = new_rdd.map(lambda record: record.idOffer)
        reference_rdd_idOffer = reference_rdd.map(lambda record: record.idOffer)

        # ATTENZIONE ritorna una lista che potrebbe essere molto grande, ma nesessario per
        # eseguire il idOffer in rdd nella riga successiva
        rdd = new_rdd_idOffer.subtract(reference_rdd_idOffer).collect()

        return new_rdd.filter(lambda record: record.idOffer in rdd)

    def extract_records_disappeared(self, new_rdd: RDD, reference_rdd: RDD) -> RDD:
        """Extract all the old records (not present in the new data) from the reference_rdd

        Args:
            new_rdd (RDD): new data
            reference_rdd (RDD): storage that keep the current status of a record

        Returns:
            RDD: all the records not in new_rdd
        """

        new_rdd_idOffer = new_rdd.map(lambda record: record.idOffer)
        reference_rdd_idOffer = reference_rdd.map(lambda record: record.idOffer)

        # ATTENZIONE ritorna una lista che potrebbe essere molto grande, ma nesessario per
        # eseguire il idOffer in rdd nella riga successiva
        rdd = reference_rdd_idOffer.subtract(new_rdd_idOffer).collect()

        return reference_rdd.filter(lambda record: record.idOffer in rdd)

    def extract_shared_records(self, new_rdd: RDD, reference_rdd: RDD) -> RDD:
        """Extract all the shared records (same idOffer) from the reference_rdd and new_rdd

        Args:
            new_rdd (RDD): new data
            reference_rdd (RDD): storage that keep the current status of a record

        Returns:
            RDD: records shared
        """
        new_rdd_idOffer = new_rdd.map(lambda record: record.idOffer)
        reference_rdd_idOffer = reference_rdd.map(lambda record: record.idOffer)
        shared_ids_rdd = new_rdd_idOffer.intersection(reference_rdd_idOffer).map(
            lambda id: (id, None)
        )

        new_rdd_filtered = new_rdd.map(lambda record: (record.idOffer, record)).join(
            shared_ids_rdd
        )
        reference_rdd_filtered = reference_rdd.map(
            lambda record: (record.idOffer, record)
        ).join(shared_ids_rdd)

        rdd = reference_rdd_filtered.union(new_rdd_filtered)
        rdd_no_keys = rdd.map(lambda record: record[1])
        rdd_no_none_values = rdd_no_keys.map(lambda record: record[0])

        return rdd_no_none_values

    def same_records_diff(self, rdd: RDD) -> RDD:
        """Perform the comparison between same records and produce a new one that describes the changes(if happened)

        Args:
            rdd (RDD): data on which perform the calculations

        Returns:
            RDD: data with the status updated and unique idOffer
        """
        return (
            rdd.map(lambda record: (record.idOffer, record))
            .reduceByKey(lambda a, b: a - b if a.date > b.date else b - a)
            .map(lambda t: t[1])
        )

    def get_just_updates(self, new_reference_rdd: RDD, old_reference_rdd: RDD) -> RDD:
        """Get just the updated records

        Args:
            new_reference_rdd (RDD): current status of the global table of car records
            old_reference_rdd (RDD): old status of the global table of car records

        Returns:
            RDD: the record updated from old_reference to new_reference
        """

        def evaluate(record):

            old_status = record[1][0].status
            new_status = record[1][1].status

            if (
                old_status == "DUPLICATED"
                and new_status == "CLOSED"
                or old_status == "REOPEN"
                and new_status == "OPEN"
            ):  # semanticamente la variazione non è rilevante quindi ignorata
                return False
            elif old_status != new_status:
                return True
            else:
                return False

        old_key_value = old_reference_rdd.map(lambda record: (record.idOffer, record))
        new_key_value = new_reference_rdd.map(lambda record: (record.idOffer, record))

        diff = (
            old_key_value.join(new_key_value)
            .filter(evaluate)
            .map(lambda record: record[1][1])
        )

        return diff

    def get_just_new_disappeared(self, old_reference_rdd: RDD, disappeared: RDD) -> RDD:
        """Get just new disappeared records

        Args:
            old_reference_rdd (RDD): old status of the global table of car records
            disappeared (RDD): all current disappeared car records

        Returns:
            RDD: only new disappeared records
        """

        old_key_value = old_reference_rdd.map(lambda record: (record.idOffer, record))
        disappeared_key_value = disappeared.map(lambda record: (record.idOffer, record))

        def evaluate(record):
            old_status = record[1][0].status
            new_status = record[1][1].status

            return old_status != new_status

        diff = (
            old_key_value.join(disappeared_key_value)
            .filter(evaluate)
            .map(lambda record: record[1][1])
        )

        return diff
